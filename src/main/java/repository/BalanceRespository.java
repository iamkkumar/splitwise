package repository;

import lombok.Getter;
import pojos.Balance;
import pojos.UserMoney;

import java.util.*;

@Getter
public class BalanceRespository {
    private Map<String, List<UserMoney>> balanceRespository;

    private BalanceRespository(){
        this.balanceRespository=new HashMap<String, List<UserMoney>>();
    }

    private static BalanceRespository instance = null;

    public static BalanceRespository getInstance() {
        if(instance == null){
            instance = new BalanceRespository();
        }
        return instance;
    }

    public void addBalance(String userId, UserMoney share) {
        List<UserMoney> shares;
        if (balanceRespository.containsKey(userId)) {
            shares=balanceRespository.get(userId);
            boolean flag=true;

            for(int i=0; i<shares.size(); i++){
                if(share.getUserId()==shares.get(i).getUserId()){
                    double amount=shares.get(i).getAmount();
                    shares.get(i).setAmount(amount+share.getAmount());
                    flag=false;
                    break;
                }
            }

            if(flag){
                shares.add(share);
            }
        }
        else{
            shares=new ArrayList<>();
            shares.add(share);
        }

        balanceRespository.put(userId, shares);
    }
}
