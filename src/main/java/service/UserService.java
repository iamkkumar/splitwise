package service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pojos.User;
import repository.UserRepository;

@Getter
@Setter
@AllArgsConstructor
public class UserService {
    private UserRepository userRepository;

    private UserService(){
      this.userRepository = UserRepository.getInstance();
    }
    private volatile static UserService instance = null;

    public static UserService getInstance(){
       if(instance == null){
           instance = new UserService();
       }
       return  instance;
    }

    public void addUser(User user){
        this.userRepository.addUser(user);
    }
}
