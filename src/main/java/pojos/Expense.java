package pojos;

import enums.ExpenseType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Expense {
    private String paidBy;
    private Double amount;
    private ExpenseType expenseType;
    private List<String> splitBetween;
    private List<Double> splitPortions;
}
