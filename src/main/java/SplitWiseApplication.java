import enums.ExpenseType;
import pojos.Expense;
import pojos.User;
import service.BalanceService;
import service.ExpenseService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SplitWiseApplication {
    public static void main(String[] args){
        List<User> users=new ArrayList<>();
        User u1=new User("u1", "1", "12345", "u1@email.com");
        User u2=new User("u2", "2", "21345", "u2@email.com");
        User u3=new User("u3", "3", "31245", "u3@email.com");
        User u4=new User("u4", "4", "41235", "u4@email.com");
        users.add(u1);
        users.add(u2);
        users.add(u3);
        users.add(u4);

        BalanceService balanceService = new BalanceService();
        balanceService.getOverallBalance(users);
        balanceService.getBalanceByUser(u1);

        Expense e1= new Expense();
        e1.setPaidBy("1");
        e1.setAmount((double) 1000);
        e1.setExpenseType(ExpenseType.EQUALLY);
        e1.setSplitBetween(Arrays.asList("1", "2", "3", "4"));

        ExpenseService expenseService = new ExpenseService();
        expenseService.processExpense(e1);
        balanceService.getBalanceByUser(u4);
        balanceService.getBalanceByUser(u1);

        Expense e2= new Expense();
        e2.setPaidBy("1");
        e2.setAmount((double) 1250);
        e2.setExpenseType(ExpenseType.EXACT);
        e2.setSplitBetween(Arrays.asList("2", "3"));
        e2.setSplitPortions(Arrays.asList((double) 370, (double) 880));
        expenseService.processExpense(e2);
        balanceService.getOverallBalance(users);

        Expense e3= new Expense();
        e3.setPaidBy("4");
        e3.setAmount((double) 1200);
        e3.setExpenseType(ExpenseType.PERCENTAGE);
        e3.setSplitBetween(Arrays.asList("1", "2", "3", "4"));
        e3.setSplitPortions(Arrays.asList((double) 40, (double) 20, (double) 20, (double) 20));
        expenseService.processExpense(e3);

        balanceService.getBalanceByUser(u1);
        balanceService.getOverallBalance(users);

    }
}
