package repository;

import lombok.Getter;
import lombok.Setter;
import pojos.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class UserRepository {
    private List<User> usersList;

    private Map<String, User> usersIdMap;

    private UserRepository(){
        this.usersIdMap = new HashMap<>();
        this.usersList = new ArrayList<>();
    }

    private static UserRepository instance = null;
    public static UserRepository getInstance() {
       if(instance == null){
            instance = new UserRepository();
       }
       return instance;
    }

    public void addUser(User user) {
        if (usersIdMap.containsKey(user.getUserId())) {
            return;
        }
        usersList.add(user);
        usersIdMap.put(user.getUserId(), user);
    }

    public List<String> getAllUserIds() {
        return (List<String>) usersIdMap.keySet();
    }

    public User getUserById(String userId) {
        if(usersIdMap.containsKey(userId))
         return usersIdMap.get(userId);
        return  null;
    }
}
