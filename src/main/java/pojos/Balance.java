package pojos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Balance {
    private String lenderUserId;
    private String borrowerUserId;
    private Double balance;
}
