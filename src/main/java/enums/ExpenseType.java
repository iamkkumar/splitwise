package enums;

public enum ExpenseType {
    EXACT, EQUALLY, PERCENTAGE;
}
