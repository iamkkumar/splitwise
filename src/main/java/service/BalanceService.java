package service;

import lombok.Getter;
import lombok.Setter;
import pojos.User;
import pojos.UserMoney;
import repository.BalanceRespository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class BalanceService {
    private BalanceRespository balanceRepository;

    public BalanceService(){
        this.balanceRepository=BalanceRespository.getInstance();
    }

    public void getBalanceByUser(User user) {
        String userId = user.getUserId();
        Map<String, List<UserMoney>> balanceRespository = balanceRepository.getBalanceRespository();
        Map<String, Double> result = new HashMap<>();

        if(balanceRespository.containsKey(userId)) {
            List<UserMoney> shares=balanceRespository.get(userId);
            for(int i=0; i<shares.size(); i++){
                String otherUserId=shares.get(i).getUserId();
                Double amount=shares.get(i).getAmount();
                result.put(otherUserId, amount);
            }
        }

        for (Map.Entry<String, List<UserMoney>> mapElement : balanceRespository.entrySet()) {
            String otherUserId = mapElement.getKey();

            if(otherUserId != userId){
                List<UserMoney> shares = mapElement.getValue();
                for(int i=0; i<shares.size(); i++){
                    String curId=shares.get(i).getUserId();
                    Double amount=shares.get(i).getAmount();
                    if(result.containsKey(otherUserId)){
                        result.put(otherUserId, result.get(otherUserId)-amount);
                    }
                    else{
                        result.put(otherUserId, -amount);
                    }
                }
            }
        }

//        if(result.size()==0){
//            System.out.println("No balances");
//            return;
//        }

        for (Map.Entry<String, Double> mapElement : result.entrySet()) {
            double amount=mapElement.getValue();
            String otherUserId = mapElement.getKey();

            if(amount>0){
                System.out.println("User"+otherUserId + " owes User"+userId + ": " + amount);
            }
            else{
                System.out.println("User"+userId + " owes User"+otherUserId + ": " + Math.abs(amount));
            }
        }
    }

    public void getOverallBalance(List<User> users){
        for(int i=0; i<users.size(); i++){
            getBalanceByUser(users.get(i));
        }
    }
}
