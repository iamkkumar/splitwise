package service;

import enums.ExpenseType;
import pojos.Balance;
import pojos.Expense;
import pojos.User;
import pojos.UserMoney;
import repository.BalanceRespository;

import java.util.ArrayList;
import java.util.List;

public class ExpenseService {
    private BalanceRespository balanceRepository;

    public ExpenseService(){
        this.balanceRepository=BalanceRespository.getInstance();
    }

    private volatile static ExpenseService instance = null;

    public static ExpenseService getInstance(){
        if(instance==null){
            instance = new ExpenseService();
        }
        return instance;
    }

    public void processExpense(Expense expense){
        if(isValidExpense(expense)){
            List<UserMoney> shares = getShares(expense);
            for(int i=0; i<shares.size(); i++){
                addBalance(expense.getPaidBy(), shares.get(i));
            }
        }
    }

    private List<UserMoney> getShares(Expense expense) {
        List<UserMoney> shares=new ArrayList<>();
        List<String> splitBetween=expense.getSplitBetween();
        double amount=expense.getAmount();
        int totalPeople = splitBetween.size();

        if(expense.getExpenseType().equals(ExpenseType.EQUALLY)){
            for(int i=1; i<totalPeople; i++){
                UserMoney userMoney = new UserMoney();
                userMoney.setUserId(splitBetween.get(i));
                userMoney.setAmount(amount/totalPeople);
                shares.add(userMoney);
            }
        }

        List<Double> splitPortions = expense.getSplitPortions();

        if(expense.getExpenseType().equals(ExpenseType.PERCENTAGE)){
            for(int i=1; i<totalPeople; i++){
                double fraction=splitPortions.get(i)/100;
                UserMoney userMoney = new UserMoney();
                userMoney.setUserId(splitBetween.get(i));
                userMoney.setAmount(amount*fraction);
                shares.add(userMoney);
            }
        }

        if(expense.getExpenseType().equals(ExpenseType.EXACT)){
            for(int i=0; i<totalPeople; i++){
                UserMoney userMoney = new UserMoney();
                userMoney.setUserId(splitBetween.get(i));
                userMoney.setAmount(splitPortions.get(i));
                shares.add(userMoney);
            }
        }

        return shares;
    }

    private boolean isValidExpense(Expense expense) {
        if(expense.getExpenseType().equals(ExpenseType.EXACT)){
            return true;
        }

        List<Double> splitPortions = expense.getSplitPortions();
        if(expense.getExpenseType().equals(ExpenseType.PERCENTAGE)){
            double totalPercentage=0;
            for(int i=0; i<splitPortions.size(); i++){
                totalPercentage+=splitPortions.get(i);
            }

            if(totalPercentage==100){
                return true;
            }
            return false;
        }

        if(expense.getExpenseType().equals(ExpenseType.EXACT)){
            double totalAmount=0;
            for(int i=0; i<splitPortions.size(); i++){
                totalAmount+=splitPortions.get(i);
            }

            if(totalAmount==expense.getAmount()){
                return true;
            }
            return false;
        }

        return true;
    }

    public void addBalance(String userId, UserMoney share){
        this.balanceRepository.addBalance(userId, share);
    }
}
